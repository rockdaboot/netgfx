/*
 * Copyright (C) 2018 Tim Rühsen
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 *
 * Compilation and usage example
 *   gcc -shared -fPIC netgfx.c -o netgfx.so -ldl
 *   rm netgfx.out
 *   LD_PRELOAD=~/src/netgfx/netgfx.so <your command>
 *   cat netgfx.out
 *
 * Format of output
 *   1. timestamp, milliseconds since 1.1.1970
 *   2. socket domain (AF_INET | AF_INET6)
 *   3. socket type (SOCK_STEAM | SOCK_DGRAM)
 *   4. r | w (r=read, w=write)
 *   5. number of bytes read or written
 *
 */

#define _GNU_SOURCE
#include <sched.h>
#include <dlfcn.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdbool.h>
#include <sys/procfs.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>

#define countof(a) ((int)(sizeof(a)/sizeof(*(a))))
#define PRINTF_FORMAT(a, b) __attribute__ ((format (printf, a, b)))

static void PRINTF_FORMAT(1,2) trace_printf(const char *fmt, ...)
{
	int fd;
	va_list args;

	if ((fd = open("netgfx.out", O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) == -1)
		return;

	va_start(args, fmt);
	vdprintf(fd, fmt, args);
	va_end(args);

	close(fd);
}

static const char *get_cmdline(void)
{
	static char cmdline[2048];
	int fd, n;

	if ((fd = open("/proc/self/cmdline", O_RDONLY)) == -1)
		return NULL;

	if ((n = read(fd, cmdline, sizeof(cmdline) - 1)) < 0)
		n = 0;

	close(fd);

	cmdline[n] = 0;

	while (--n >= 0)
		if (!cmdline[n])
			cmdline[n] = ' ';

	return cmdline;
}

static struct timeval t1, t2;
static pid_t starter_pid;

static long long ms(struct timeval *t)
{
	return t->tv_sec * 1000LL + t->tv_usec / 1000;
}

static long long dur_ms(struct timeval *t1, struct timeval *t2)
{
	return (t2->tv_sec - t1->tv_sec) * 1000LL + (t2->tv_usec - t1->tv_usec) / 1000;
}

static long long cur_ms(void)
{
	static struct timeval t;

	gettimeofday(&t, NULL);
	return dur_ms(&t1, &t);
}


static int (*libc_socket)(int domain, int type, int protocol);
static int (*libc_close)(int fd);
static ssize_t (*libc_read)(int fd, void *buf, size_t count);
static ssize_t (*libc_recv)(int fd, void *buf, size_t count, int flags);
static ssize_t (*libc_recvfrom)(int fd, void *buf, size_t count, int flags, struct sockaddr *src_addr, socklen_t *addrlen);
static ssize_t (*libc_recvmsg)(int fd, struct msghdr *msg, int flags);
static int (*libc_recvmmsg)(int fd, struct mmsghdr *msgvec, unsigned int vlen, int flags, struct timespec *timeout);
static ssize_t (*libc_write)(int fd, const void *buf, size_t count);
static ssize_t (*libc_send)(int fd, const void *buf, size_t count, int flags);
static ssize_t (*libc_sendto)(int fd, const void *buf, size_t count, int flags, const struct sockaddr *src_addr, socklen_t addrlen);
static ssize_t (*libc_sendmsg)(int fd, const struct msghdr *msg, int flags);
static int (*libc_sendmmsg)(int fd, struct mmsghdr *msgvec, unsigned int vlen, int flags);

static struct sockinfo {
	int domain;
	int type;
	bool active;
} sockinfo[1024];

int socket(int domain, int type, int protocol)
{
	int fd = libc_socket(domain, type, protocol);

	if ((domain == AF_INET || domain == AF_INET6) && (type == SOCK_STREAM || type == SOCK_DGRAM)) {
		if (fd >= 0 && fd < countof(sockinfo)) {
			sockinfo[fd].domain = domain;
			sockinfo[fd].type = type;
			sockinfo[fd].active = true;
		}
	}

	return fd;
}

int close(int fd)
{
	if (fd >= 0 && fd < countof(sockinfo)) {
		sockinfo[fd].active = false;
	}

	return libc_close(fd);
}

ssize_t read(int fd, void *buf, size_t count)
{
	ssize_t ret = libc_read(fd, buf, count);

	if (fd >= 0 && fd < countof(sockinfo) && sockinfo[fd].active && ret > 0) {
		trace_printf("%lld %d %d r %zd\n", cur_ms(), sockinfo[fd].domain, sockinfo[fd].type, ret);
	}

	return ret;
}

ssize_t recv(int fd, void *buf, size_t count, int flags)
{
	ssize_t ret = libc_recv(fd, buf, count, flags);

	if (fd >= 0 && fd < countof(sockinfo) && sockinfo[fd].active && ret > 0) {
		trace_printf("%lld %d %d r %zd\n", cur_ms(), sockinfo[fd].domain, sockinfo[fd].type, ret);
	}

	return ret;
}

ssize_t recvfrom(int fd, void *buf, size_t count, int flags, struct sockaddr *src_addr, socklen_t *addrlen)
{
	ssize_t ret = libc_recvfrom(fd, buf, count, flags, src_addr, addrlen);

	if (fd >= 0 && fd < countof(sockinfo) && sockinfo[fd].active && ret > 0) {
		trace_printf("%lld %d %d r %zd\n", cur_ms(), sockinfo[fd].domain, sockinfo[fd].type, ret);
	}

	return ret;
}

ssize_t recvmsg(int fd, struct msghdr *msg, int flags)
{
	ssize_t ret = libc_recvmsg(fd, msg, flags);

	if (fd >= 0 && fd < countof(sockinfo) && sockinfo[fd].active && ret > 0) {
		trace_printf("%lld %d %d r %zd\n", cur_ms(), sockinfo[fd].domain, sockinfo[fd].type, ret);
	}

	return ret;
}

int recvmmsg(int fd, struct mmsghdr *msgvec, unsigned int vlen, int flags, struct timespec *timeout)
{
	ssize_t ret = libc_recvmmsg(fd, msgvec, vlen, flags, timeout);

	if (fd >= 0 && fd < countof(sockinfo) && sockinfo[fd].active && ret > 0) {
		size_t nbytes = 0;

		for (int it = 0; it < ret; it++)
			nbytes += msgvec[it].msg_len;

		trace_printf("%lld %d %d r %zu\n", cur_ms(), sockinfo[fd].domain, sockinfo[fd].type, nbytes);
	}

	return ret;
}

ssize_t write(int fd, const void *buf, size_t count)
{
	ssize_t ret = libc_write(fd, buf, count);

	if (fd >= 0 && fd < countof(sockinfo) && sockinfo[fd].active && ret > 0) {
		trace_printf("%lld %d %d w %zd\n", cur_ms(), sockinfo[fd].domain, sockinfo[fd].type, ret);
	}

	return ret;
}

ssize_t send(int fd, const void *buf, size_t count, int flags)
{
	ssize_t ret = libc_send(fd, buf, count, flags);

	if (fd >= 0 && fd < countof(sockinfo) && sockinfo[fd].active && ret > 0) {
		trace_printf("%lld %d %d w %zd\n", cur_ms(), sockinfo[fd].domain, sockinfo[fd].type, ret);
	}

	return ret;
}

ssize_t sendto(int fd, const void *buf, size_t count, int flags, const struct sockaddr *src_addr, socklen_t addrlen)
{
	ssize_t ret = libc_sendto(fd, buf, count, flags, src_addr, addrlen);

	if (fd >= 0 && fd < countof(sockinfo) && sockinfo[fd].active && ret > 0) {
		trace_printf("%lld %d %d w %zd\n", cur_ms(), sockinfo[fd].domain, sockinfo[fd].type, ret);
	}

	return ret;
}

ssize_t sendmsg(int fd, const struct msghdr *msg, int flags)
{
	ssize_t ret = libc_sendmsg(fd, msg, flags);

	if (fd >= 0 && fd < countof(sockinfo) && sockinfo[fd].active && ret > 0) {
		trace_printf("%lld %d %d w %zd\n", cur_ms(), sockinfo[fd].domain, sockinfo[fd].type, ret);
	}

	return ret;
}

int sendmmsg(int fd, struct mmsghdr *msgvec, unsigned int vlen, int flags)
{
	ssize_t ret = libc_sendmmsg(fd, msgvec, vlen, flags);

	if (fd >= 0 && fd < countof(sockinfo) && sockinfo[fd].active && ret > 0) {
		size_t nbytes = 0;

		for (int it = 0; it < ret; it++)
			nbytes += msgvec[it].msg_len;

		trace_printf("%lld %d %d r %zu\n", cur_ms(), sockinfo[fd].domain, sockinfo[fd].type, nbytes);
	}

	return ret;
}

// Is not called on fork/clone, just on exec().
static void __attribute__((constructor)) start(void)
{
	gettimeofday(&t1, NULL);

	libc_socket = dlsym(RTLD_NEXT, "socket");
	libc_close = dlsym(RTLD_NEXT, "close");
	libc_read = dlsym(RTLD_NEXT, "read");
	libc_recv = dlsym(RTLD_NEXT, "recv");
	libc_recvfrom = dlsym(RTLD_NEXT, "recvfrom");
	libc_recvmsg = dlsym(RTLD_NEXT, "recvmsg");
	libc_recvmmsg = dlsym(RTLD_NEXT, "recvmmsg");
	libc_write = dlsym(RTLD_NEXT, "write");
	libc_send = dlsym(RTLD_NEXT, "send");
	libc_sendto = dlsym(RTLD_NEXT, "sendto");
	libc_sendmsg = dlsym(RTLD_NEXT, "sendmsg");
	libc_sendmmsg = dlsym(RTLD_NEXT, "sendmmsg");
}

// Is always called before process stops - so it doesn't pair up with
// the constructor.
static void __attribute__((destructor)) printstats(void)
{
	struct rusage u;
	long long duration, utime = 0, stime = 0;

	gettimeofday(&t2, NULL);
	duration = dur_ms(&t1, &t2); // real duration in ms

	if (getrusage(RUSAGE_SELF, &u) == 0) {
		utime = ms(&u.ru_utime);
		stime = ms(&u.ru_stime);
	}

//	trace_printf("%lld %d %d %lld %lld %lld %s\n",
//		ms(&t2), getpid(), getppid(), duration, utime, stime, get_cmdline());
}
