#!/bin/bash
#
# Copyright (C) 2018 Tim Rühsen
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
#
# The diagram is Mbps averaged over 0.1s

title="HTTPS via HTTP/1.1"
wget_options="-q --no-config -O/tmp/xxx -r"
wget_version=$(wget --version|head -1|cut -d' ' -f3)
wget2_options="-q --no-config -O/dev/null -r"
wget2_version=$(~/src/wget2/src/wget2_noinstall --version 2>/dev/null|head -1|cut -d' ' -f3)
curl_options="-s -o/dev/null --cert-status --http1.1 -o/dev/null"
curl_version=$(curl --version|head -1|cut -d' ' -f2)

urls="https://example.com"

rm -f netgfx.out
LD_PRELOAD=./netgfx.so wget ${wget_options} $urls
rm -rf /tmp/xxx
mv netgfx.out wget.data
awk '{ t[int($1/100)]+=$5 } END { for (i in t) print i*100/1000 " " (t[i]*10*8)/1000000 }' wget.data | sort -g >wget_total.data

rm -f netgfx.out
LD_PRELOAD=./netgfx.so ~/src/wget2/src/wget2_noinstall ${wget2_options} $urls
mv netgfx.out wget2.data
awk '{ t[int($1/100)]+=$5 } END { for (i in t) print i*100/1000 " " (t[i]*10*8)/1000000 }' wget2.data | sort -g >wget2_total.data


cat <<EOF | gnuplot
  set terminal svg
  set output "$0.svg"

  set title "${title}\n\
 500MBit/s Cable\n\
 wget ${wget_version} options: ${wget_options}\n\
 wget2 ${wget2_version} options: ${wget2_options}\n"
 #curl ${curl_version} options: ${curl_options}"

  # aspect ratio for image size
  #set size 1,0.7

  # y-axis grid
  set grid y

  # x-axis label
  set xlabel "Time in s"

  #y-axis label
  set ylabel "mbps"

  plot \
    "wget_total.data" using 1:2 with boxes title "wget", \
    "wget2_total.data" using 1:2 with boxes title "wget2"
EOF
