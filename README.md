netgfx
======

A small GNU/Linux tool to collect stats of IP traffic of a command.

The output can be converted into a diagram to display on web pages
or just for comparison.

The idea was to fire up a network command (wget, curl, ...) and thereafter
have a diagram of the bandwidth utilization.

Netgfx just needs user privileges.

Building
--------

		git clone git@gitlab.com:rockdaboot/netgfx.git
		cd netgfx
		gcc -shared -fPIC netgfx.c -o netgfx.so -ldl

Usage example
-------------

		rm netgfx.out
		LD_PRELOAD=~/src/netgfx/netgxf.so wget -q -O/dev/null https://example.com

		# Aggregate all traffic by time (output format: ms bytes)
		awk '{ t[$1]+=$5 } END { for (i in t) print i " " t[i]*8 }' netgfx.out >netgfx_total.data

There is a script `plot.sh` which is tiny an example how to plot the bandwidth by time
and output the gfx as a SVG file.

Format of output
----------------

Fields seperated by space.

1. timestamp, milliseconds relative to start of command
2. socket domain (AF_INET (=2) | AF_INET6 (=10))
3. socket type (SOCK_STREAM (=1) | SOCK_DGRAM (=2))
4. r | w (r=read, w=write)
5. number of bytes read or written

Example output
--------------

Here is the output when I run `plot.sh` against my homepage.

![wget vs. wget2 recursive download](/uploads/afb6eac0becf8907ae187f1faa45e13b/myhomepage.svg)
